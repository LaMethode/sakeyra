## schema collections mongodb

### Environment
```json
{
  "name":"<nom>",
  "short":"<shortname>",
}
```

### Remote user
```json
{
"name":"<nom utilisateur>",
"email":"<email utilisateur>",
"pubkey":"<cle publique>",
"env": [
  "env1",
  "env2",
  "env3"
]
}
```

### Local user:
```json
{
"name":"<nom utilisateur>",
"pubkey":"<cle publique>",
"env": [
  "env1",
  "env2",
  "env3"
]
}
```

### Bundle
```json
{
  "environment": "rec",
  "matchlist": [
    {
       "pubkey": "sgdrfhdfgjfgkhklddfqsdgqsgqsg",
       "local": "root",
       "email": "userremote2@test.com"
     },
     {
       "pubkey": "sgdrfhdfgjfgkhklddfqsdgqsgqsg",
       "local": "maintenance",
       "email": "userremote2@test.com"
     },
     {
       "pubkey": "sgdrfhdfgjfgkhklddfqsdgqsgqsg",
       "local": "sauvegarde",
       "email": "userremote2@test.com"
     },
     {
       "pubkey": "sdqgsfghsdfhsdg",
       "local": "test",
       "email": "userremote1@test.com"
     }
  ],
  "pub_date": "2017-10-18T17:15:46.799689"
}
```
