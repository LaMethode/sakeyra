#!/usr/bin/python3
# -*- coding: utf-8 -*-

from io import open
from os import path
from setuptools import setup, find_packages

import sakeyra

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='sakeyra',
    version=sakeyra.__version__,
    author="Jeremy Collin",
    author_email="jeremy.collin.@protonmail.com",
    description="Service to distribute SSH Public keys",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/LaMethode/sakeyra',
    license="GPLv3",
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 4 - Beta",
        "Environment :: Console",
        "Natural Language :: French",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Intended Audience :: System Administrators",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",  # noqa
        "Topic :: System :: Systems Administration",
    ],
    packages=find_packages(),
    # package_dir={'':''},
    install_requires=[
        "Flask==1.0.2",
        "Flask-Bootstrap==3.3.7.1",
        "Flask-WTF==0.14.2",
        "PyJWT>=1.7.1",
        "setuptools>=40.9",
        "TinyDB==3.13.0",
        "waitress>=1.2.1",
        "Werkzeug>=0.14",
        "WTForms==2.2.1"
        ],
    include_package_data=True,
    entry_points={'console_scripts': ['sakeyra = sakeyra.sakeyra:main']},
)
