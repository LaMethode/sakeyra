#!/bin/bash

_sortie(){
  if [ "${ERR}" != "0" ]; then
    exit 1
  fi
}

_usage(){
  echo "Usage: ${0} -f, pour lancer avec flask"
  echo "Usage: ${0} -w, pour lancer avec waitress"
  ERR=1
  _sortie
}

function run_flask(){
  export FLASK_APP=sakeyra
  export FLASK_ENV=development
  flask run --host=0.0.0.0
}

function run_waitress(){
  waitress-serve --port='5000' --host='0.0.0.0' --call 'sakeyra:create_app'
}

if [ -z "${1}" ]; then
  ERR=1
  _usage
fi

while getopts "fw" OPT
do
  case "${OPT}" in
    f ) run_flask;;
    w ) run_waitress;;
    h ) _usage;;
    * ) _usage;;
  esac
done
