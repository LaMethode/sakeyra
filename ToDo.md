TODO:
- [x] Change to TinyDB from MongoDB (SQLite)
- [x] Docstrings
- [ ] Real login screen
- [ ] Forge unittests
- [ ] Get info from clients
- [ ] Make a list of clients/environments
- [ ] Make a log file

TODO-CI:
- [ ] Gitlab CI

TODO-Packaging:
- [ ] Packaging for PyPi
- [ ] Packaging for Docker
