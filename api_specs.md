## API specs

### remoteUser

#### get all remote users
- ```-H "Content-Type: application/json" -X GET /api/remoteusers```

#### get one specific remote user
- ```-H "Content-Type: application/json" -X GET /api/remoteusers/<id>```

##### create remote user
- ```-H "Content-Type: application/json" -X POST /api/remoteusers -d '{}'```

### localUser

#### get all local users
- ```-H "Content-Type: application/json" -X GET /api/localusers```

#### get one specific local user
- ```-H "Content-Type: application/json" -X GET /api/localusers/<id>```

##### create local user
- ```-H "Content-Type: application/json" -X POST /api/localusers -d '{}'```

### environment

#### get all environments
- ```-H "Content-Type: application/json" -X GET /api/environments```

#### get one specific environments
- ```-H "Content-Type: application/json" -X GET /api/environments/<id>```

##### create environments
- ```-H "Content-Type: application/json" -X POST /api/environments -d '{}'```
