"""
    Sakeyra::auth
    Module de gestion des authentifications
"""
import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from .db import SakDB


BP = Blueprint('auth', __name__, url_prefix='/auth')


def login_required(view):
    """ You have to log to see this """
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        return view(**kwargs)
    return wrapped_view


@BP.route('/register', methods=('GET', 'POST'))
@login_required
def register():
    """ You should register yourself """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        _db = SakDB()
        user = _db.search_one('users', 'username', username)
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'
        elif user:
            error = 'User {} is already registered.'.format(username)
        if error is None:
            _db.create_user(username, generate_password_hash(password))
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html')


@BP.route('/login', methods=('GET', 'POST'))
def login():
    """ Log me in Scotty!! """
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        _db = SakDB()
        error = None
        user = _db.search_one('users', 'username', username)
        if not user:
            error = 'Incorrect username.'
        elif not check_password_hash(user[0]['hpasswd'], password):
            error = 'Incorrect password.'
        if error is None:
            session.clear()
            session['user_id'] = user[0]['id']
            return redirect(url_for('base.index'))

        flash(error)

    return render_template('auth/login.html')


@BP.before_app_request
def load_logged_in_user():
    """ You are who you logged """
    user_id = session.get('user_id')
    _db = SakDB()
    if user_id is None:
        g.user = None
    else:
        g.user = _db.search_one('users', 'id', id)


@BP.route('/logout')
def logout():
    """ Bye bye """
    session.clear()
    return redirect(url_for('base.index'))
