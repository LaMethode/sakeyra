"""
    Sakeyra::db
    Module de gestion de la bdd TinyDb
"""
import datetime
import uuid
from tinydb import TinyDB, Query


class SakDB:
    """
        Gestion de la BDD sakeyra
    """

    def __init__(self):
        """ Connection to database """
        self._db = TinyDB('./sakeyra.json')
        self.table = ''
        self.data = ''

    def get_data(self, table):
        """ Get all data
            from specified table
        """
        self.table = self._db.table(table).all()
        if table != 'bundle':
            self.data = sorted(self.table, key=lambda k: k['id'])
        else:
            self.data = self.table
        return self.data

    def search_one(self, table, filtre, item):
        """ Get one item
            from specified table
        """
        one = self._db.table(table).search(Query()[filtre] == item)
        return one

    def get_lastbundle(self, environment):
        """ Get the last bundle of defined environment
            find all <env> bundle from last to first
            get last bundle
            return a dict(bundle)
        """
        env = self._db.table('environment').search(
            Query()['short'] == environment
        )
        bundle = self._db.table('bundle').search(
            Query()['environment'] == env[0]['name']
            )
        array = sorted(
            bundle, key=lambda k: k['pub_date'], reverse=True
            )
        return array[0]

    def set_local(self, login, pubkey, env):
        """ Create a dict(user)
            from user and public key
        """
        local_user = dict()
        local_user['id'] = str(uuid.uuid1())
        local_user['login'] = login
        local_user['pubkey'] = pubkey
        local_user['env'] = env
        dblocals = self._db.table('local')
        dblocals.insert(local_user)
        return local_user

    def set_remote(self, name, email, pubkey, env):
        """ Create a dict(keys)
            from email, name and public ssh key
        """
        remote_user = dict()
        remote_user['id'] = str(uuid.uuid1())
        remote_user['email'] = email
        remote_user['name'] = name
        remote_user['pubkey'] = pubkey
        remote_user['env'] = env
        dbremotes = self._db.table('remote')
        dbremotes.insert(remote_user)
        return remote_user

    def set_env(self, name, short):
        """ Create a dict(environment)
            from a longname (name) and a shortname (short)
        """
        env = dict()
        env['id'] = str(uuid.uuid1())
        env['short'] = short
        env['name'] = name
        dbenv = self._db.table('environment')
        dbenv.insert(env)
        return env

    def set_bundle(self, bundle):
        """ Create a dict(bundle) which contains
            publication date,
            environment,
            users and associated public keys
        """
        bundle['id'] = str(uuid.uuid1())
        bundle['pub_date'] = datetime.datetime.today().isoformat()
        dbbundle = self._db.table('bundle')
        dbbundle.insert(bundle)
        return bundle

    def update_local(self, user):
        """ update info of local user """
        local_user = dict()
        local_user['id'] = user['id']
        local_user['login'] = user['login']
        local_user['pubkey'] = user['pubkey']
        uplocal = self._db.table('local')
        uplocal.update(local_user, Query()['id'] == local_user['id'])
        return local_user

    def update_remote(self, user):
        """ update info of remote user """
        remote_user = dict()
        remote_user['id'] = user['id']
        remote_user['name'] = user['name']
        remote_user['email'] = user['email']
        remote_user['pubkey'] = user['pubkey']
        remote_user['env'] = user['envs']
        upremote = self._db.table('remote')
        upremote.update(remote_user, Query()['id'] == remote_user['id'])
        return remote_user

    def del_data(self, table, filtre, item):
        """ Delete one item
            from specified table
        """
        deletion = self._db.table(table).remove(Query()[filtre] == item)
        return deletion

    def create_user(self, username, password):
        """ create user that can use the app """
        user = dict()
        user['id'] = str(uuid.uuid1())
        user['username'] = username
        user['hpasswd'] = password
        dbusers = self._db.table('users')
        dbusers.insert(user)
        return user
