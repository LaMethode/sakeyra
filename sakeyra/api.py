"""
    Sakeyra::api
    Module de gestion des appels webservices
"""
import json
import jwt
from flask import Blueprint, jsonify
from flask import current_app as app
from .db import SakDB


BP = Blueprint('api', __name__, url_prefix='/api')


# Recupération des données API
@BP.route('/<environment>')
def getlastbundle(environment):
    """ Partie API:
        Affiche le bundle demandé:
        argument 'environment',
        affiche le bundle le plus récent pour
        cet environment.
    """
    data = SakDB()
    secret_key = app.config['SECRET_KEY']
    print(secret_key)
    try:
        data.get_lastbundle(environment)
    except IndexError:
        message = {"message": "this environment does not exist, yet."}
        return jsonify(message)

    if secret_key:
        payload = json.dumps(data.get_lastbundle(environment))
        jsondata = jwt.encode(
            json.loads(payload),
            secret_key,
            algorithm='HS256'
        )
        jsonpl = {"payload": jsondata}
    else:
        jsonpl = data.get_lastbundle(environment)
    return jsonify(jsonpl)


@BP.route('/health')
def get_health():
    """ Check health
        fonction de verification que l'appli tourne bien
        A FAIRE
    """
    health = {"health": "Okay"}
    return jsonify(health)
