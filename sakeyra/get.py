"""
    Sakeyra::get
    Module de lecture des données UI
"""
from flask import render_template, Blueprint
from .db import SakDB


BP = Blueprint('get', __name__, url_prefix='/list')


@BP.route('/<item>', methods=['GET'])
def get_list(item):
    """ Flask: affiche la liste de toutes
        les données du type demandé dans l'IHM
    """
    data = SakDB()
    datas = data.get_data(item)
    return render_template("list/item.html", datas=datas, item=item)


@BP.route('/bundle', methods=['GET'])
def get_bundle():
    """ Flask: affiche la liste de toutes
        les données du type demandé dans l'IHM
    """
    data = SakDB()
    datas = data.get_data('bundle')
    return render_template("list/bundle.html", datas=datas)
