"""
    Sakeyra::forms
    Module de gestion des formulaires WTForms
"""
from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, HiddenField
from wtforms.validators import DataRequired, Email, Required


class DelForm(FlaskForm):
    """
        Formulaire de suppression de données
    """
    id = BooleanField('Nom', validators=[Required()])


class SetLocalUserForm(FlaskForm):
    """
        Formulaire de creation d'utilisateur local
    """
    login = StringField('login', validators=[DataRequired()])
    pubkey = TextAreaField('Cle publique', validators=[DataRequired()])
    envs = StringField('Environment', validators=[DataRequired()])


class UpdateLocalUserForm(FlaskForm):
    """
        Formulaire de modification d'utilisateur local
    """
    id = HiddenField('id', validators=[DataRequired()])
    login = HiddenField('login', validators=[DataRequired()])
    pubkey = TextAreaField('Cle publique', validators=[DataRequired()])
    envs = StringField('Environment', validators=[DataRequired()])


class SetRemoteUserForm(FlaskForm):
    """
        Formulaire de creation d'utilisateur distant
    """
    name = StringField('Nom', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    pubkey = TextAreaField('Cle publique', validators=[DataRequired()])
    envs = StringField('Environment', validators=[DataRequired()])


class UpdateRemoteUserForm(FlaskForm):
    """
        Formulaire de modification d'utilisateur distant
    """
    id = HiddenField('id', validators=[DataRequired()])
    name = HiddenField('Nom', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    pubkey = TextAreaField('Cle publique', validators=[DataRequired()])
    envs = StringField('Environment', validators=[DataRequired()])


class EnvForm(FlaskForm):
    """
        Formulaire de creation d'environnement
    """
    name = StringField('Nom', validators=[DataRequired()])
    short = StringField('Nom court', validators=[DataRequired()])


class BundleForm(FlaskForm):
    """
        Formulaire de creation de bundle de clés ssh
    """
    env = BooleanField('Environment', validators=[DataRequired()])
    local = StringField('Local Users', validators=[DataRequired()])
    remote = StringField('Remote Users', validators=[DataRequired()])
