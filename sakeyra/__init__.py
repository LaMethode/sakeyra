"""
    Sakeyra::__init__
    Module de base de l'application Sakeyra
"""
import os
from flask import Flask
from flask_bootstrap import Bootstrap

__version__ = "0.2.0"

# pylint: disable=R0903
class BaseConfig():
    """ Configuration de Flask """
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'e03f76b91a607dab4d83b3fd8419bc895da9f1f5567e2d67ce8eac95b144'
    BOOTSTRAP_SERVE_LOCAL = True


BASE_CONFIG = BaseConfig()


def create_app(test_config=None):
    """ Creation de l'application """
    app = Flask(__name__, instance_relative_config=False)
    Bootstrap(app)

    if test_config is None:
        app.config.from_object(BaseConfig)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import base, auth, get, post, api
    app.register_blueprint(base.BP)
    app.register_blueprint(auth.BP)
    app.register_blueprint(get.BP)
    app.register_blueprint(post.BP)
    app.register_blueprint(api.BP)

    return app
