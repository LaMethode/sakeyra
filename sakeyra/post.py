"""
    Sakeyra::post
    Module d'insertion des données UI/DB
"""
import json
from flask import flash, redirect, render_template, request, Blueprint

from .db import SakDB
from .auth import login_required
from .forms import (
    SetLocalUserForm,
    SetRemoteUserForm,
    UpdateLocalUserForm,
    UpdateRemoteUserForm,
    EnvForm,
    BundleForm,
    DelForm
)


BP = Blueprint('post', __name__)  # , url_prefix='/set')


@BP.route('/del/<item>', methods=['GET', 'POST'])
@login_required
def del_item(item):
    """ Flask: Affiche une liste d'items demandés
        avec la possibilité de les supprimer
    """
    data = SakDB()
    datas = data.get_data(item)
    form = DelForm(request.form)
    if request.method == 'POST':
        ids = request.form.getlist('id')
        for ident in ids:
            data.del_data(item, 'id', ident)
        return redirect("/list/{item}".format(item=item))
    return render_template(
        "edit/del_item.html", datas=datas, item=item, form=form
        )


@BP.route('/set/local', methods=['GET', 'POST'])
@login_required
def setlocal():
    """ Ajout d'utilsateurs locaux """
    data = SakDB()
    form = SetLocalUserForm(request.form)
    envslist = data.get_data('environment')
    if request.method == 'POST':
        login = request.form.get('login')
        pubkey = request.form.get('pubkey')
        envs = []
        for env in request.form.getlist('envs'):
            envs.append(env)
        data.set_local(login, pubkey, envs)
        return redirect('/list/local')
    return render_template(
        'edit/f_local.html',
        form=form, envslist=envslist,
        title='Create New Local user')


@BP.route('/update/local', methods=['GET', 'POST'])
@login_required
def updatelocal():
    """ Mise à jour d'utilsateurs locaux """
    data = SakDB()
    form = UpdateLocalUserForm(request.form)
    local_user = dict()
    localdata = data.get_data('local')
    if request.method == 'POST':
        local_user['id'] = request.form.get('id')
        local_user['login'] = request.form.get('login')
        local_user['pubkey'] = request.form.get('pubkey')
        data.update_local(local_user)
        return redirect('/list/local')
    return render_template(
        'edit/u_local.html',
        form=form,
        locals=localdata,
        title='Update local user')


@BP.route('/update/remote', methods=['GET', 'POST'])
@login_required
def updateremote():
    """ Mise à jour d'utilsateurs distants """
    data = SakDB()
    envslist = data.get_data('environment')
    form = UpdateRemoteUserForm(request.form)
    remotes = data.get_data('remote')
    remote_user = dict()
    if request.method == 'POST':
        remote_user['id'] = request.form.get('id')
        remote_user['name'] = request.form.get('name')
        remote_user['email'] = request.form.get('email')
        remote_user['pubkey'] = request.form.get('pubkey')
        remote_user['envs'] = []
        for env in request.form.getlist('envs'):
            remote_user['envs'].append(env)
        data.update_remote(remote_user)
        return redirect('/list/remote')
    return render_template(
        'edit/u_remote.html',
        form=form,
        remotes=remotes,
        envslist=envslist,
        title='Update remote user')


@BP.route('/set/remote', methods=['GET', 'POST'])
@login_required
def setremote():
    """ Ajout d'utilsateurs distants """
    data = SakDB()
    form = SetRemoteUserForm(request.form)
    envslist = data.get_data('environment')
    if request.method == 'POST':
        name = request.form.get('name')
        email = request.form.get('email')
        pubkey = request.form.get('pubkey')
        envs = []
        for env in request.form.getlist('envs'):
            envs.append(env)
        data.set_remote(name, email, pubkey, envs)
        return redirect('/list/remote')
    return render_template(
        'edit/f_remote.html',
        form=form, envslist=envslist,
        title='Create New Remote user')


@BP.route('/set/environment', methods=['GET', 'POST'])
@login_required
def setenv():
    """ Creation d'environnements """
    form = EnvForm()
    if request.method == 'POST' and form.validate_on_submit():
        flash(
            'name = "%s", short = "%s"' %
            (form.name.data, form.short.data))
        name = form.name.data
        short = form.short.data
        data = SakDB()
        data.set_env(name, short)
        return redirect('/list/environment')
    return render_template(
        'edit/f_env.html', form=form, title='Create New Environment')


@BP.route('/set/bundle', methods=['GET', 'POST'])
@login_required
def setbundle():
    """
        Creation de paquets de clés
        par utiliseurs locaux
        et par environnements
    """
    form = BundleForm(request.form)
    data = SakDB()
    locallist = data.get_data('local')
    remotelist = data.get_data('remote')
    envslist = data.get_data('environment')
    bundle = dict()
    if request.method == 'POST':
        env = request.form.get('env')
        localdata = request.form.getlist('local')
        bundle['environment'] = env
        bundle['matchlist'] = []
        for local in localdata:
            jlocal = json.loads(local)
            bundle['matchlist'].append(jlocal)
        data.set_bundle(bundle)
        flash('Un bundle <em>{env}</em> a été créé.'.format(env=env))
        return redirect('/list/bundle')
    return render_template(
        'edit/f_bundle.html', form=form,
        locals=locallist, envs=envslist, remotes=remotelist)
