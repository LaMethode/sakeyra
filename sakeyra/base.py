"""
    Sakeyra::base
    Module des urls de base
"""
from flask import (
    Blueprint, render_template, request
)


BP = Blueprint('base', __name__, url_prefix='/')


@BP.route('/')
def index():
    """ Flask: definition de la page racine
    IHM d'acceuil
    """
    return render_template('index.html', title='Sakeyra')


@BP.errorhandler(404)
def page404():
    """ Flask: affichage spécifique:
        ERREUR 404
    """
    path = request.path
    return render_template(
        'fourzerofour.html', path=path, title='You are lost!')
