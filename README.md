# Sakeyra :cherry_blossom: :snake:
## Server for authorized keys remote admin

A tool to manage ~/.ssh/authorized_keys on multiple servers and environments.  
Works with [Akeyra](https://github.com/LaMethode/akeyra):cherry_blossom:
